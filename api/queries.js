const path = require('path');

const movies = require(path.join(__dirname, '../data/movies.json'));

const { Pool } = require('pg');

const pool = new Pool({
  host: 'localhost',
  port: 5432,
  user: 'postgres',
  password: 'Reva@043',
  database: 'movies'
});

/*inserting records into movies database.*/
const directorsTable = async () => {
  try {
    const client = await pool.connect();

    // inserting records into directors table
    movies
      .reduce((director, element) => {
        director.includes(element.Director) || director.push(element.Director);
        return director;
      }, [])
      .forEach(async element => {
        await client.query(`insert into directors (directorname) values($1)`, [
          element
        ]);
      });
    client.release();
  } catch (err) {
    console.log(err.stack);
  }
};

const moviesTable = async () => {
  try {
    const client = await pool.connect();
    // inserting records into movies table.
    movies.forEach(async element => {
      const directorIds = await client.query(
        `select directorid from directors where directorname = ($1)`,
        [element.Director]
      );
      await client.query(
        `insert into movies (movietitle,directorid) values ($1, $2)`,
        [element.Title, directorIds.rows[0].directorid]
      );
    });
    client.release();
  } catch (err) {
    console.log(err.stack);
  }
};

/*inserting records into movies database.*/
// directorsTable();
// moviesTable();
