const express = require('express');
// const movies = require(`${__dirname}` + '/controllers/moviesRoutes.js');
const directors = require(`${__dirname}` + '/controllers/directorRoutes.js');

const app = express();

// app.use('/api/movies', movies);
app.use('/api/directors', directors);

const port = process.env.port || 3000;
app.listen(port, () => console.log(`Listening on port ${port}`));
