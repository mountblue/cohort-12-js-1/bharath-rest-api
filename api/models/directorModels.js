const { Pool } = require('pg');

const pool = new Pool({
  host: 'localhost',
  port: 5432,
  user: 'postgres',
  password: 'Reva@043',
  database: 'movies',
  max: 10,
  idleTimeoutMillis: 1
});

const getAllDirectors = async () => {
  const client = await pool.connect();
  const allDirectors = await client.query('select * from directors');
  client.release();
  return allDirectors;
};

module.exports = { getAllDirectors };
