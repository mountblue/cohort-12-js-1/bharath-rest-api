const express = require('express');
const router = express.Router();

const {
  getAllDirectors
  //   getDirectorById
  //   insertDirectorById,
  //   updateDirector,
  //   deleteDirector
} = require('../models/directorModels.js');

router.use(express.json());

router.get('/', async (req, res) => {
  const allDirectors = await getAllDirectors();
  res.json(allDirectors.rows);
});

module.exports = router;
